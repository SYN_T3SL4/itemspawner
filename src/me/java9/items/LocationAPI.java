package me.java9.items;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.Serializable;

public class LocationAPI implements Serializable {

    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;
    private String world;

    public LocationAPI(Location loc){
        x = loc.getX();
        y = loc.getY();
        z = loc.getZ();
        yaw = loc.getYaw();
        pitch = loc.getPitch();
        world = loc.getWorld().getName();
    }

    public Location getLocation(){
        return new Location(Bukkit.getWorld(world),x,y,z,yaw,pitch);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public String getWorld() {
        return world;
    }

    public void setWorld(String world) {
        this.world = world;
    }
}
