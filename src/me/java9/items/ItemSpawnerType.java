package me.java9.items;

import java.io.Serializable;

public class ItemSpawnerType implements Serializable{

    int id;
    int data = 0;

    public ItemSpawnerType(int id, int data){
       this.id = id;
       this.data = data;
    }

    public ItemSpawnerType(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getData() {
        return data;
    }
}
