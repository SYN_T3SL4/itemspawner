package me.java9.items;

import com.sainttx.holograms.api.Hologram;
import com.sainttx.holograms.api.HologramManager;
import com.sainttx.holograms.api.HologramPlugin;
import com.sainttx.holograms.api.line.HologramLine;
import com.sainttx.holograms.api.line.TextLine;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.TileEntityMobSpawner;
import net.minecraft.server.v1_8_R3.World;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class ItemSpawnerPlugin extends JavaPlugin implements Listener{

    @Override
    public void onEnable() {
        SettingsManager.getInstance().setup(this);
    }

    @Override
    public void onDisable() {
        SettingsManager.getInstance().stop();
        getServer().getPluginManager().registerEvents(this,this);
    }

    public void setSpawner(Block block,Player p) {
        Location loc = block.getLocation();
        World world = ((CraftWorld)block.getWorld()).getHandle();

        BlockPosition blockPos = new BlockPosition(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());

        TileEntityMobSpawner spawner = (TileEntityMobSpawner) world.getTileEntity(blockPos);

        NBTTagCompound spawnerTag = new NBTTagCompound();
        spawner.b(spawnerTag);

        spawnerTag.remove("SpawnPotentials");
        spawnerTag.setString("EntityId", "Item");

        NBTTagCompound itemTag = new NBTTagCompound();

        itemTag.setShort("Health", (short)5);
        itemTag.setShort("Age", (short)0);
        ItemStack items = p.getItemInHand();
        ItemStack item = new ItemStack(351, 1, (short)0, (byte)4);
        p.setItemInHand(item);
        p.updateInventory();
        net.minecraft.server.v1_8_R3.ItemStack itemStack = CraftItemStack.asNMSCopy((CraftItemStack)p.getItemInHand());
        NBTTagCompound itemStackTag = new NBTTagCompound();
        itemStack.save(itemStackTag);
        itemStackTag.setByte("Count", (byte)1);
        itemTag.set("Item", itemStackTag);
        spawnerTag.set("SpawnData", itemTag);
        spawnerTag.setShort("SpawnCount", (short)itemStack.count);
        spawnerTag.setShort("SpawnRange", (short)100);
        spawner.a(spawnerTag);
        p.setItemInHand(items);
        p.updateInventory();
    }

    public static ItemSpawnerPlugin getPlugin(){
        return (ItemSpawnerPlugin) Bukkit.getPluginManager().getPlugin("ItemSpawner");
    }
}
