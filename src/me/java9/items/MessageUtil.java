package me.java9.items;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;

public class MessageUtil {

    public static String PREFIX;
    public static String CONFIG_RELOADED;
    public static String NO_PERM_MESSAGE;
    public static String PLAYER_NOT_FOUND;
    public static String ERROR_MESSAGE;
    public static String DEVRET_KAPALI;
    public static String KONSOL_KOMUT;
    public static String HOLOGRAM_LINE_1;
    public static String HOLOGRAM_LINE_2;

    public static String INFO_LINE_1;
    public static String INFO_LINE_2;
    public static String INFO_LINE_3;
    public static String INFO_LINE_4;
    public static String INFO_LINE_5;
    public static String INFO_LINE_6;
    public static String INFO_LINE_7;
    public static String INFO_LINE_8;
    public static String INFO_LINE_9;

    static SettingsManager manager = SettingsManager.getInstance();

    public static void loadMessages() {
        PREFIX = manager.chatcolor(manager.config.getConfig().getString("PREFIX"));

        CONFIG_RELOADED = PREFIX + manager.chatcolor(manager.config.getConfig().getString("CONFIG_RELOADED"));
        NO_PERM_MESSAGE = PREFIX + manager.chatcolor(manager.config.getConfig().getString("NO_PERM_MESSAGE"));
        PLAYER_NOT_FOUND = PREFIX + manager.chatcolor(manager.config.getConfig().getString("PLAYER_NOT_FOUND"));
        ERROR_MESSAGE = PREFIX + manager.chatcolor(manager.config.getConfig().getString("ERROR_MESSAGE"));
        DEVRET_KAPALI = PREFIX + manager.chatcolor(manager.config.getConfig().getString("DEVRET_KAPALI"));
        KONSOL_KOMUT = PREFIX + manager.chatcolor(manager.config.getConfig().getString("KONSOL_KOMUT"));
        HOLOGRAM_LINE_1 = PREFIX + manager.chatcolor(manager.config.getConfig().getString("HOLOGRAM_LINE_1"));
        HOLOGRAM_LINE_2 = PREFIX + manager.chatcolor(manager.config.getConfig().getString("HOLOGRAM_LINE_2"));

        INFO_LINE_1 = manager.chatcolor(manager.config.getConfig().getString("INFO_LINE_1"));
        INFO_LINE_2 = PREFIX + manager.chatcolor(manager.config.getConfig().getString("INFO_LINE_2"));
        INFO_LINE_3 = PREFIX + manager.chatcolor(manager.config.getConfig().getString("INFO_LINE_3"));
        INFO_LINE_4 = PREFIX + manager.chatcolor(manager.config.getConfig().getString("INFO_LINE_4"));
        INFO_LINE_5 = PREFIX + manager.chatcolor(manager.config.getConfig().getString("INFO_LINE_5"));
        INFO_LINE_6 = PREFIX + manager.chatcolor(manager.config.getConfig().getString("INFO_LINE_6"));
        INFO_LINE_7 = PREFIX + manager.chatcolor(manager.config.getConfig().getString("INFO_LINE_7"));
        INFO_LINE_8 = PREFIX + manager.chatcolor(manager.config.getConfig().getString("INFO_LINE_8"));
        INFO_LINE_9 = PREFIX + manager.chatcolor(manager.config.getConfig().getString("INFO_LINE_9"));
    }
}
