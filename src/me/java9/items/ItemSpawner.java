package me.java9.items;

import java.io.Serializable;

public class ItemSpawner implements Serializable {

    private String owner;
    private int id;
    private int lvl = 1;
    private LocationAPI loc = null;
    private ItemSpawnerType type;

    public ItemSpawner(String owner, int type, int data) {
        this.owner = owner;
        this.type = new ItemSpawnerType(type,data);
        SettingsManager.getInstance().itemspawner.add(this);
    }

    public int getHiz(){
        SettingsManager m = SettingsManager.getInstance();
        if (lvl == m.yukseltme.getConfig().getInt("Level1.Hiz")) {
            return m.yukseltme.getConfig().getInt("Level1.Hiz");
        } else if (lvl == m.yukseltme.getConfig().getInt("Level2.Hiz")) {
            return m.yukseltme.getConfig().getInt("Level2.Hiz");
        }  else if (lvl == m.yukseltme.getConfig().getInt("Level3.Hiz")) {
            return m.yukseltme.getConfig().getInt("Level3.Hiz");
        }  else if (lvl == m.yukseltme.getConfig().getInt("Level4.Hiz")) {
            return m.yukseltme.getConfig().getInt("Level4.Hiz");
        }  else if (lvl == m.yukseltme.getConfig().getInt("Level5.Hiz")) {
            return m.yukseltme.getConfig().getInt("Level5.Hiz");
        }  else if (lvl == m.yukseltme.getConfig().getInt("Level6.Hiz")) {
            return m.yukseltme.getConfig().getInt("Level6.Hiz");
        }  else if (lvl == m.yukseltme.getConfig().getInt("Level7.Hiz")) {
            return m.yukseltme.getConfig().getInt("Level7.Hiz");
        }  else if (lvl == m.yukseltme.getConfig().getInt("Level8.Hiz")) {
            return m.yukseltme.getConfig().getInt("Level8.Hiz");
        }  else if (lvl == m.yukseltme.getConfig().getInt("Level9.Hiz")) {
            return m.yukseltme.getConfig().getInt("Level9.Hiz");
        }  else if (lvl == m.yukseltme.getConfig().getInt("Level10.Hiz")) {
            return m.yukseltme.getConfig().getInt("Level10.Hiz");
        }
        return 1;
    }

    public ItemSpawnerType getType() {
        return type;
    }

    public void setType(ItemSpawnerType type) {
        this.type = type;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public LocationAPI getLoc() {
        return loc;
    }

    public void setLoc(LocationAPI loc) {
        this.loc = loc;
    }
}
