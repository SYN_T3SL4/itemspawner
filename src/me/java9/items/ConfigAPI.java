package me.java9.items;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;

public class ConfigAPI {

    private File configFile;
    private String filename;
    private FileConfiguration fileConfiguration;

    public ConfigAPI(JavaPlugin plugin, String configName, Boolean shouldCopy) {
        filename = configName + ".yml";
        configFile = new File(plugin.getDataFolder(), filename);
        if (shouldCopy.booleanValue()) {
            firstRun(plugin);
        }
        fileConfiguration = YamlConfiguration.loadConfiguration(configFile);

        load();
    }

    public FileConfiguration getConfig() {
        load();
        return fileConfiguration;
    }

    public void save() {
        try {
            fileConfiguration.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            fileConfiguration.load(configFile);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void firstRun(JavaPlugin plugin) {
        if (!configFile.exists())
        {
            configFile.getParentFile().mkdirs();
            copy(plugin.getResource(filename), configFile);
        }
    }

    private void copy(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[63];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception localException) {}
    }

    public ConfigurationSection getConfigurationSection(String path) {
        return getConfig().getConfigurationSection(path);
    }
}
