package me.java9.items;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

import com.sainttx.holograms.api.HologramManager;
import com.sainttx.holograms.api.HologramPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class SettingsManager {

    static SettingsManager instance = new SettingsManager();
    ConfigAPI config;
    ConfigAPI yukseltme;
    ConfigAPI spawners;
    ItemSpawnerPlugin isp;
    ArrayList<ItemSpawner> itemspawner = new ArrayList<>();
    private HologramManager hologramManager;

    public static SettingsManager getInstance() {
        return instance;
    }

    public void setup(ItemSpawnerPlugin isp) {
        this.isp = isp;
        hologramManager = JavaPlugin.getPlugin(HologramPlugin.class).getHologramManager();
        config = new ConfigAPI(isp, "ayarlar", true);
        yukseltme = new ConfigAPI(isp,"yukseltme",true);
        spawners = new ConfigAPI(isp,"spawners",true);
        MessageUtil.loadMessages();
        try {
            File file = new File(isp.getDataFolder() + "/data/");
            file.mkdirs();
            itemspawner = SL.load(isp.getDataFolder() + "/data/itemspawner.yml");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        registerCommands();
        registerListener();
    }

    public void stop(){
        try {
            File file = new File(isp.getDataFolder() + "/data/");
            file.mkdirs();
            SL.save(itemspawner, isp.getDataFolder() + "/data/itemspawner.yml");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private void registerCommands() {
        isp.getCommand("itemsp").setExecutor(new ItemSpawnerCommand());
    }

    private void registerListener(Listener... listeners) {
        Arrays.stream(listeners).forEach(listener -> isp.getServer().getPluginManager().registerEvents(listener,isp));
    }

    public String chatcolor(String s) {
        return ChatColor.translateAlternateColorCodes('&',s);
    }
}