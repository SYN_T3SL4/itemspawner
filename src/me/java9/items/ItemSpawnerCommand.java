package me.java9.items;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Objects;

public class ItemSpawnerCommand implements CommandExecutor {

    SettingsManager manager = SettingsManager.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
        if (args.length > 0) {
            if (Objects.equals(args[0], "help")) {
                if (sender.isOp()) {
                    sender.sendMessage(MessageUtil.INFO_LINE_1);
                    sender.sendMessage(MessageUtil.INFO_LINE_2);
                    sender.sendMessage(MessageUtil.INFO_LINE_3);
                    sender.sendMessage(MessageUtil.INFO_LINE_4);
                    sender.sendMessage(MessageUtil.INFO_LINE_5);
                    sender.sendMessage(MessageUtil.INFO_LINE_6);
                    sender.sendMessage(MessageUtil.INFO_LINE_7);
                    sender.sendMessage(MessageUtil.INFO_LINE_8);
                    sender.sendMessage(MessageUtil.INFO_LINE_9);
                } else {
                    sender.sendMessage(MessageUtil.INFO_LINE_1);
                    sender.sendMessage(MessageUtil.INFO_LINE_7);
                    sender.sendMessage(MessageUtil.INFO_LINE_8);
                    sender.sendMessage(MessageUtil.INFO_LINE_9);
                }
            } else if (Objects.equals(args[0], "reload")) {
                if (sender.isOp()) {
                    manager.config.load();
                    MessageUtil.loadMessages();
                    manager.spawners.load();
                    manager.yukseltme.load();
                    sender.sendMessage(MessageUtil.CONFIG_RELOADED);
                } else {
                    sender.sendMessage(MessageUtil.NO_PERM_MESSAGE);
                }
            } else if (Objects.equals(args[0], "kurtar")) {
                //kurtar gui si
            } else if (Objects.equals(args[0], "devret")) {
                if (manager.config.getConfig().getBoolean("SPAWNER_DEVRET_AKTIF")) {

                } else {
                    sender.sendMessage(MessageUtil.DEVRET_KAPALI);
                }
                //devretme guisi
            } else if (args.length > 1) {
                if (Objects.equals(args[0], "devret")) {
                    if (manager.config.getConfig().getBoolean("SPAWNER_DEVRET_AKTIF")) {
                        if (Bukkit.getPlayer(args[1]) != null) {
                            Player p = Bukkit.getPlayer(args[1]);

                            //elindekini oyuncuya devredicek
                            //oyuncuya mesaj gidicek config ile ayarlı
                        } else {
                            sender.sendMessage(MessageUtil.PLAYER_NOT_FOUND.replaceAll("%player%", args[1]));
                        }
                    } else {
                        sender.sendMessage(MessageUtil.DEVRET_KAPALI);
                    }
                }
            }
        } else {
            if (sender.isOp()) {
                sender.sendMessage(MessageUtil.INFO_LINE_1);
                sender.sendMessage(MessageUtil.INFO_LINE_2);
                sender.sendMessage(MessageUtil.INFO_LINE_3);
                sender.sendMessage(MessageUtil.INFO_LINE_4);
                sender.sendMessage(MessageUtil.INFO_LINE_5);
                sender.sendMessage(MessageUtil.INFO_LINE_6);
                sender.sendMessage(MessageUtil.INFO_LINE_7);
                sender.sendMessage(MessageUtil.INFO_LINE_8);
                sender.sendMessage(MessageUtil.INFO_LINE_9);
            } else {
                sender.sendMessage(MessageUtil.INFO_LINE_1);
                sender.sendMessage(MessageUtil.INFO_LINE_7);
                sender.sendMessage(MessageUtil.INFO_LINE_8);
                sender.sendMessage(MessageUtil.INFO_LINE_9);
            }
        }
        return false;
    }
}
